//
//  NewPacksViewController.h
//  Contacts Minder
//
//  Created by Subash Luitel on 7/7/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LensData.h"
#import "AppDelegate.h"

@interface NewPacksViewController : UIViewController

@property (strong, nonatomic) NSArray *packsArray;
@property (strong, nonatomic) IBOutlet UISlider *leftEyeSlider;
- (IBAction)leftEyeChangePacks:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *leftEyePacks;
@property (strong, nonatomic) IBOutlet UISlider *rightEyeSlider;
- (IBAction)rightEyeChangePacks:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *rightEyePacks;
@property (strong, nonatomic) IBOutlet UIButton *buyButton;
- (IBAction)buyButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
- (IBAction)cancelButtonPressed:(id)sender;
@property (retain, nonatomic) AppDelegate *appDelegate;

@end
