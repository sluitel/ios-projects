//
//  LossViewController.m
//  Contacts Minder
//
//  Created by Subash Luitel on 7/16/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import "LossViewController.h"

@interface LossViewController ()

@end

@implementation LossViewController {
    int lensesLeft;
    int lensesRight;
}

@synthesize leftEyeSlider, leftEyeLabel, rightEyeLabel, rightEyeSlider, appDelegate, reportButton, cancelButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    reportButton.backgroundColor = [UIColor brownColor];
    cancelButton.backgroundColor = [UIColor brownColor];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (IBAction)leftEyeSliderChanged:(id)sender {
    int currentValue;
    NSString *valueChange = [[NSString alloc] init];
    currentValue = [leftEyeSlider value];
    lensesLeft = currentValue;
    if (currentValue == 0) {
        valueChange = [NSString stringWithFormat:@"none"];
    }
    else if (currentValue == 1) {
        valueChange = [NSString stringWithFormat:@"%i Lens",currentValue];
    }
    else {
        valueChange = [NSString stringWithFormat:@"%i Lenses",currentValue];
    }
    leftEyeLabel.text = valueChange;
}

- (IBAction)rightEyeSliderChanged:(id)sender {
    int currentValue;
    NSString *valueChange = [[NSString alloc] init];
    currentValue = [rightEyeSlider value];
    lensesRight = currentValue;
    if (currentValue == 0) {
        valueChange = [NSString stringWithFormat:@"none"];
    }
    else if (currentValue == 1) {
        valueChange = [NSString stringWithFormat:@"%i Lens",currentValue];
    }
    else {
        valueChange = [NSString stringWithFormat:@"%i Lenses",currentValue];
    }
    rightEyeLabel.text = valueChange;
}

- (IBAction)reportButtonPressed:(id)sender {
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    // Retrieve the entity from the local store -- much like a table in a database
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LensData" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    // Set the predicate -- much like a WHERE statement in a SQL database
    NSNumber *check = [[NSNumber alloc] initWithInt:1];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", check];
    [request setPredicate:predicate];
    
    // Set the sorting -- mandatory, even if you're fetching a single record/object
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    // Request the data -- NOTE, this assumes only one match, that
    // yourIdentifyingQualifier is unique. It just grabs the first object in the array.
    
    LensData *data = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
    lensesRight = [data.lensesRight intValue] - lensesRight;
    lensesLeft = [data.lensesLeft intValue] - lensesLeft;
    
    data.lensesRight = [NSNumber numberWithInt:lensesRight];
    data.lensesLeft = [NSNumber numberWithInt:lensesLeft];
    [context save:&error];
    
    [self performSegueWithIdentifier:@"lossBackward" sender:self];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"lossBackward" sender:self];
}
@end
