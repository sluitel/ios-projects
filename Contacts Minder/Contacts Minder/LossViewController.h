//
//  LossViewController.h
//  Contacts Minder
//
//  Created by Subash Luitel on 7/16/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "LensData.h"

@interface LossViewController : UIViewController
@property (strong, nonatomic) IBOutlet UISlider *leftEyeSlider;
- (IBAction)leftEyeSliderChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *leftEyeLabel;
@property (strong, nonatomic) IBOutlet UISlider *rightEyeSlider;
- (IBAction)rightEyeSliderChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *rightEyeLabel;
- (IBAction)reportButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;
@property (retain, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIButton *reportButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;

@end
