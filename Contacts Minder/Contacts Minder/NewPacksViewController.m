//
//  NewPacksViewController.m
//  Contacts Minder
//
//  Created by Subash Luitel on 7/7/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import "NewPacksViewController.h"

@interface NewPacksViewController ()

@end

@implementation NewPacksViewController {
    int lensesLeft;
    int lensesRight;
}

@synthesize packsArray, leftEyeSlider, leftEyePacks, rightEyePacks, rightEyeSlider, appDelegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    _buyButton.backgroundColor = [UIColor brownColor];
    _cancelButton.backgroundColor = [UIColor brownColor];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (IBAction)leftEyeChangePacks:(id)sender {
    int currentValue;
    NSString *valueChange = [[NSString alloc] init];
    currentValue = [leftEyeSlider value];
    lensesLeft = currentValue * 6;
    if (currentValue == 0) {
        valueChange = [NSString stringWithFormat:@"none"];
    }
    else if (currentValue == 1) {
        valueChange = [NSString stringWithFormat:@"%i Pack",currentValue];
    }
    else {
        valueChange = [NSString stringWithFormat:@"%i Packs",currentValue];
    }
    leftEyePacks.text = valueChange;
}

- (IBAction)rightEyeChangePacks:(id)sender {
    int currentValue;
    NSString *valueChange = [[NSString alloc] init];
    currentValue = [rightEyeSlider value];
    lensesRight = currentValue * 6;
    if (currentValue == 0) {
        valueChange = [NSString stringWithFormat:@"none"];
    }
    else if (currentValue == 1) {
        valueChange = [NSString stringWithFormat:@"%i Pack",currentValue];
    }
    else {
        valueChange = [NSString stringWithFormat:@"%i Packs",currentValue];
    }
    rightEyePacks.text = valueChange;
}

- (IBAction)buyButtonPressed:(id)sender {
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    // Retrieve the entity from the local store -- much like a table in a database
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LensData" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    // Set the predicate -- much like a WHERE statement in a SQL database
    NSNumber *check = [[NSNumber alloc] initWithInt:1];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", check];
    [request setPredicate:predicate];
    
    // Set the sorting -- mandatory, even if you're fetching a single record/object
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    // Request the data -- NOTE, this assumes only one match, that
    // yourIdentifyingQualifier is unique. It just grabs the first object in the array.
    
    LensData *data = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
    lensesRight += [data.lensesRight intValue];
    lensesLeft += [data.lensesLeft intValue];
    
    data.lensesRight = [NSNumber numberWithInt:lensesRight];
    data.lensesLeft = [NSNumber numberWithInt:lensesLeft];
    [context save:&error];

    [self performSegueWithIdentifier:@"doneBuyingSegue" sender:self];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"doneBuyingSegue" sender:self];
}
@end
