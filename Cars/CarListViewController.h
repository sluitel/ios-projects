//
//  CarListViewController.h
//  Cars
//
//  Created by Subash Luitel on 2/25/13.
//  Copyright (c) 2013 Illinois Institute of Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarListViewController : UITableViewController

@end
