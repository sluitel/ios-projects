//
//  WebViewController.h
//  Nepal Khabar
//
//  Created by Subash Luitel on 5/22/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>
@property (strong, nonatomic) NSString *address;

@property (strong, nonatomic) IBOutlet UIWebView *webView;


@end
