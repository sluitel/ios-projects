//
//  ViewController.h
//  EcoCouture
//
//  Created by Subash Luitel on 5/10/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface HomeViewController : UIViewController {
    IBOutlet UIScrollView *scroller;
}
@property (nonatomic, retain) UIScrollView *scroller;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, strong) NSMutableString *webAddress;
@property (nonatomic, strong) NSMutableString *webTitle;
- (IBAction)shareButton:(id)sender;
@end
