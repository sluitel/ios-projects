//
//  WebViewController.h
//  EcoCouture
//
//  Created by Subash Luitel on 5/10/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) NSMutableString *address;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)refreshButton:(id)sender;

@end
