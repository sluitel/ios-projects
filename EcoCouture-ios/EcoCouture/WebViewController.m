//
//  WebViewController.m
//  EcoCouture
//
//  Created by Subash Luitel on 5/10/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController
@synthesize webView, address;


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest: request];
}


- (IBAction)refreshButton:(id)sender {
    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest: request];
}
@end
