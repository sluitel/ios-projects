//
//  ViewController.m
//  EcoCouture
//
//  Created by Subash Luitel on 5/10/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController () <UIScrollViewDelegate>

@end

@implementation HomeViewController
@synthesize scroller, imageView, webAddress, webTitle;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    //set scrollView
    CGRect bounds = [[UIScreen mainScreen] bounds];
    scroller = [[UIScrollView alloc] initWithFrame:bounds];
    [scroller setContentSize:CGSizeMake(320, 1120)];
    [scroller setScrollEnabled:YES];
    
    //Title Image
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 213)];
    imageView.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"carousel-item-1.jpg"],
                       [UIImage imageNamed:@"carousel-item-2.jpg"],
                       [UIImage imageNamed:@"carousel-item-3.jpg"],
                       [UIImage imageNamed:@"carousel-item-4.jpg"],
                       [UIImage imageNamed:@"cover.jpg"], nil];
    imageView.animationDuration = 10;
    [imageView startAnimating];
    
    //10% off image
    UIImageView *tenOff = [[UIImageView alloc] initWithFrame:CGRectMake(5, 218, 152.5, 95)];
    tenOff.image = [UIImage imageNamed:@"savebylike.jpg"];
    
    //10% off Button
    UIButton *tenOffButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tenOffButton.frame = CGRectMake(5, 218, 152.5, 95);
    [tenOffButton addTarget:self action:@selector(tenOff) forControlEvents:UIControlEventTouchUpInside];
    
    //Athletic wear Image
    UIImageView *athletic = [[UIImageView alloc] initWithFrame:CGRectMake(163, 218, 152.5, 95)];
    athletic.image = [UIImage imageNamed:@"two.png"];
    
    //Melissa Image
    UIImageView *melissa = [[UIImageView alloc] initWithFrame:CGRectMake(5, 318, 240, 150)];
    melissa.image = [UIImage imageNamed:@"mell.jpg"];
    
    //Melissa Shop Button
    UIButton *melissaShopButton = [UIButton buttonWithType:UIButtonTypeCustom];
    melissaShopButton.frame = CGRectMake(247, 318, 69, 74);
    melissaShopButton.backgroundColor = [UIColor blackColor];
    [melissaShopButton setTitle:@"Shop" forState:UIControlStateNormal];
    [melissaShopButton addTarget:self action:@selector(shopMelissa) forControlEvents:UIControlEventTouchUpInside];
    
    //Melissa Follow Button
    UIButton *melissaFollowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    melissaFollowButton.frame = CGRectMake(247, 394, 69, 74);
    melissaFollowButton.backgroundColor = [UIColor blackColor];
    [melissaFollowButton setTitle:@"Follow" forState:UIControlStateNormal];
    [melissaFollowButton addTarget:self action:@selector(followMelissa) forControlEvents:UIControlEventTouchUpInside];
    
    //Marisa Image
    UIImageView *marisa = [[UIImageView alloc] initWithFrame:CGRectMake(5, 473, 240, 150)];
    marisa.image = [UIImage imageNamed:@"marisa.jpg"];
    
    //Marisa Shop Button
    UIButton *marisaShopButton = [UIButton buttonWithType:UIButtonTypeCustom];
    marisaShopButton.frame = CGRectMake(247, 473, 69, 74);
    marisaShopButton.backgroundColor = [UIColor blackColor];
    [marisaShopButton setTitle:@"Shop" forState:UIControlStateNormal];
    [marisaShopButton addTarget:self action:@selector(shopMarisa) forControlEvents:UIControlEventTouchUpInside];
        
    //Marisa Follow Button
    UIButton *marisaFollowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    marisaFollowButton.frame = CGRectMake(247, 549, 69, 74);
    marisaFollowButton.backgroundColor = [UIColor blackColor];
    [marisaFollowButton setTitle:@"Follow" forState:UIControlStateNormal];
    [marisaFollowButton addTarget:self action:@selector(followMarisa) forControlEvents:UIControlEventTouchUpInside];
    
    //Linda Image
    UIImageView *linda = [[UIImageView alloc] initWithFrame:CGRectMake(5, 473, 240, 150)];
    linda.image = [UIImage imageNamed:@"mell.jpg"];
    
    //Linda Shop Button
    UIButton *lindaShopButton = [UIButton buttonWithType:UIButtonTypeCustom];
    lindaShopButton.frame = CGRectMake(247, 318, 69, 74);
//    melissaShopButton.backgroundColor = [UIColor blackColor];
//    [melissaShopButton setTitle:@"Shop" forState:UIControlStateNormal];
//    [melissaShopButton addTarget:self action:@selector(shopMelissa) forControlEvents:UIControlEventTouchUpInside];
//    
//    //Melissa Follow Button
//    UIButton *melissaFollowButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    melissaFollowButton.frame = CGRectMake(247, 394, 69, 74);
//    melissaFollowButton.backgroundColor = [UIColor blackColor];
//    [melissaFollowButton setTitle:@"Follow" forState:UIControlStateNormal];
//    [melissaFollowButton addTarget:self action:@selector(followMelissa) forControlEvents:UIControlEventTouchUpInside];
//    
//    //Marisa Image
//    UIImageView *marisa = [[UIImageView alloc] initWithFrame:CGRectMake(5, 473, 240, 150)];
//    marisa.image = [UIImage imageNamed:@"marisa.jpg"];
//    
//    //Marisa Shop Button
//    UIButton *marisaShopButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    marisaShopButton.frame = CGRectMake(247, 473, 69, 74);
//    marisaShopButton.backgroundColor = [UIColor blackColor];
//    [marisaShopButton setTitle:@"Shop" forState:UIControlStateNormal];
//    [marisaShopButton addTarget:self action:@selector(shopMarisa) forControlEvents:UIControlEventTouchUpInside];
//    
//    //Marisa Follow Button
//    UIButton *marisaFollowButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    marisaFollowButton.frame = CGRectMake(247, 549, 69, 74);
//    marisaFollowButton.backgroundColor = [UIColor blackColor];
//    [marisaFollowButton setTitle:@"Follow" forState:UIControlStateNormal];
//    [marisaFollowButton addTarget:self action:@selector(followMarisa) forControlEvents:UIControlEventTouchUpInside];
    
    //tops image
    UIImageView *tops = [[UIImageView alloc] initWithFrame:CGRectMake(5, 628, 100, 175)];
    tops.image = [UIImage imageNamed:@"tops.jpg"];
    
    //tops Button
    UIButton *topsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    topsButton.frame = CGRectMake(5, 628, 100, 175);
    [topsButton addTarget:self action:@selector(getTops) forControlEvents:UIControlEventTouchUpInside];
    
    //tops text field
    UILabel *topsText = [[UILabel alloc] initWithFrame:CGRectMake(5, 803, 100, 25)];
    topsText.text = @"Tops";
    [topsText setTextAlignment:NSTextAlignmentCenter];
    [topsText setBackgroundColor:[UIColor brownColor]];
    [topsText setTextColor:[UIColor whiteColor]];
    
    
    //Dresses image
    UIImageView *dresses = [[UIImageView alloc] initWithFrame:CGRectMake(110, 628, 100, 175)];
    dresses.image = [UIImage imageNamed:@"dresses.jpg"];
    
    //Dresses Button
    UIButton *dressesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dressesButton.frame = CGRectMake(110, 628, 101, 175);
    [dressesButton addTarget:self action:@selector(getDresses) forControlEvents:UIControlEventTouchUpInside];
    
    //dresses text field
    UILabel *dressesText = [[UILabel alloc] initWithFrame:CGRectMake(110, 803, 100, 25)];
    dressesText.text = @"Dresses";
    [dressesText setTextAlignment:NSTextAlignmentCenter];
    [dressesText setBackgroundColor:[UIColor brownColor]];
    [dressesText setTextColor:[UIColor whiteColor]];

    //Bottoms image
    UIImageView *bottoms = [[UIImageView alloc] initWithFrame:CGRectMake(215, 628, 100, 175)];
    bottoms.image = [UIImage imageNamed:@"bottoms.jpg"];
    
    //Bottoms Button
    UIButton *bottomsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    bottomsButton.frame = CGRectMake(215, 628, 1012, 175);
    [bottomsButton addTarget:self action:@selector(getBottoms) forControlEvents:UIControlEventTouchUpInside];
    
    //bottoms text field
    UILabel *bottomsText = [[UILabel alloc] initWithFrame:CGRectMake(215, 803, 100, 25)];
    bottomsText.text = @"Bottoms";
    [bottomsText setTextAlignment:NSTextAlignmentCenter];
    [bottomsText setBackgroundColor:[UIColor brownColor]];
    [bottomsText setTextColor:[UIColor whiteColor]];
    
    //Graphic Tees image
    UIImageView *graphicTees = [[UIImageView alloc] initWithFrame:CGRectMake(5, 833, 100, 175)];
    graphicTees.image = [UIImage imageNamed:@"graph.jpg"];
    
    //graphic Tees Button
    UIButton *graphicTeesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    graphicTeesButton.frame = CGRectMake(5, 833, 1012, 175);
    [graphicTeesButton addTarget:self action:@selector(getGraphicTees) forControlEvents:UIControlEventTouchUpInside];
    
    //Graphic Tees text field
    UILabel *graphicTeesText = [[UILabel alloc] initWithFrame:CGRectMake(5, 1008, 100, 50)];
    graphicTeesText.text = @"Graphic\nTees";
    [graphicTeesText setTextAlignment:NSTextAlignmentCenter];
    [graphicTeesText setBackgroundColor:[UIColor brownColor]];
    [graphicTeesText setTextColor:[UIColor whiteColor]];
    [graphicTeesText setLineBreakMode:NSLineBreakByWordWrapping];
    graphicTeesText.numberOfLines = 0;
    
    //Melissa image
    UIImageView *melissaPolinar = [[UIImageView alloc] initWithFrame:CGRectMake(110, 833, 100, 175)];
    melissaPolinar.image = [UIImage imageNamed:@"mel.jpg"];
    
    //Melissa Button
    UIButton *melissaPolinarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    melissaPolinarButton.frame = CGRectMake(110, 833, 1012, 175);
    [melissaPolinarButton addTarget:self action:@selector(getMelissa) forControlEvents:UIControlEventTouchUpInside];
    
    //Melissa text field
    UILabel *melissaPolinarText = [[UILabel alloc] initWithFrame:CGRectMake(110, 1008, 100, 50)];
    melissaPolinarText.text = @"Melissa\nPolinar";
    [melissaPolinarText setTextAlignment:NSTextAlignmentCenter];
    [melissaPolinarText setBackgroundColor:[UIColor brownColor]];
    [melissaPolinarText setTextColor:[UIColor whiteColor]];
    [melissaPolinarText setLineBreakMode:NSLineBreakByWordWrapping];
    melissaPolinarText.numberOfLines = 0;

    //Spring 2013 image
    UIImageView *spring = [[UIImageView alloc] initWithFrame:CGRectMake(215, 833, 100, 175)];
    spring.image = [UIImage imageNamed:@"spr.jpg"];
    
    //Spring 2013 Button
    UIButton *springButton = [UIButton buttonWithType:UIButtonTypeCustom];
    springButton.frame = CGRectMake(215, 833, 1012, 175);
    [springButton addTarget:self action:@selector(getSpring) forControlEvents:UIControlEventTouchUpInside];
    
    //Spring 2013 text field
    UILabel *springText = [[UILabel alloc] initWithFrame:CGRectMake(215, 1008, 100, 50)];
    springText.text = @"Spring\n2013";
    [springText setTextAlignment:NSTextAlignmentCenter];
    [springText setBackgroundColor:[UIColor brownColor]];
    [springText setTextColor:[UIColor whiteColor]];
    [springText setLineBreakMode:NSLineBreakByWordWrapping];
    springText.numberOfLines = 0;

    
    //view Hirarchy
    [scroller addSubview:linda];
    [scroller addSubview:melissaPolinarButton];
    [scroller addSubview:melissaPolinar];
    [scroller addSubview:melissaPolinarText];
    [scroller addSubview:spring];
    [scroller addSubview:springButton];
    [scroller addSubview:springText];
    [scroller addSubview:graphicTees];
    [scroller addSubview:graphicTeesButton];
    [scroller addSubview:graphicTeesText];
    [scroller addSubview:dressesText];
    [scroller addSubview:bottomsText];
    [scroller addSubview:topsText];
    [scroller addSubview:bottoms];
    [scroller addSubview:bottomsButton];
    [scroller addSubview:dresses];
    [scroller addSubview:dressesButton];
    [scroller addSubview:tops];
    [scroller addSubview:topsButton];
    [scroller addSubview:marisaFollowButton];
    [scroller addSubview:marisaShopButton];
    [scroller addSubview:marisa];
    [scroller addSubview:melissaFollowButton];
    [scroller addSubview:melissaShopButton];
    [scroller addSubview:melissa];
    [scroller addSubview:athletic];
    [scroller addSubview:tenOff];
    [scroller addSubview:imageView];
    [scroller addSubview:tenOffButton];
    [self.view addSubview:scroller];
    
}

- (void) tenOff {
    webAddress = [[NSMutableString alloc]
                  initWithString:@"https://www.facebook.com/ecococlothing"];
    webTitle =[[NSMutableString alloc]
            initWithString:@"Get 10% Off"];
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}

- (void) followMelissa {
    [self performSegueWithIdentifier:@"melissaSegue" sender:self];
}

- (void) followMarisa {
    [self performSegueWithIdentifier:@"marisaSegue" sender:self];
}

- (void) shopMelissa {
    webAddress = [[NSMutableString alloc]
                  initWithString:@"http://www.ecocoshop.com/collections/all/melissa-polinar-for-ecocouture"];
    webTitle =[[NSMutableString alloc]
               initWithString:@"Melissa Polinar"];
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}

- (void) shopMarisa {
    webAddress = [[NSMutableString alloc]
                  initWithString:@"http://www.ecocoshop.com/collections/marisa-quinn-for-ecocouture"];
    webTitle =[[NSMutableString alloc]
               initWithString:@"Marisa Quinn"];
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}

- (void) getTops {
    webAddress = [[NSMutableString alloc]
                  initWithString:@"http://www.ecocoshop.com/collections/all/tops"];
    webTitle =[[NSMutableString alloc]
               initWithString:@"Tops"];
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}

- (void) getDresses {
    webAddress = [[NSMutableString alloc]
                  initWithString:@"http://www.ecocoshop.com/collections/all/dresses"];
    webTitle =[[NSMutableString alloc]
               initWithString:@"Dresses"];
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}

- (void) getBottoms {
    webAddress = [[NSMutableString alloc]
                  initWithString:@"http://www.ecocoshop.com/collections/all/bottoms"];
    webTitle =[[NSMutableString alloc]
               initWithString:@"Bottoms"];
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}

- (void) getGraphicTees {
    webAddress = [[NSMutableString alloc]
                  initWithString:@"http://www.ecocoshop.com/collections/all/t-shirt-designs"];
    webTitle =[[NSMutableString alloc]
               initWithString:@"Graphic Tees"];
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}

- (void) getMelissa {
    webAddress = [[NSMutableString alloc]
                  initWithString:@"http://www.ecocoshop.com/collections/all/melissa-polinar-for-ecocouture"];
    webTitle =[[NSMutableString alloc]
               initWithString:@"Melissa Poliner"];
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}

- (void) getSpring {
    webAddress = [[NSMutableString alloc]
                  initWithString:@"http://www.ecocoshop.com/collections/all/spring-2013"];
    webTitle =[[NSMutableString alloc]
               initWithString:@"Spring 2013"];
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"webSegue"]) {
        WebViewController *webVC = segue.destinationViewController;
        webVC.navigationItem.title = webTitle;
        webVC.navigationItem.backBarButtonItem.title = @"Home";
        webVC.address = webAddress;
    }
}

- (IBAction)shareButton:(id)sender {
    NSString* someText = @"I am shopping at EcoCouture. Check it out.\nhttp://www.ecocoshop.com";
    NSArray* dataToShare = @[someText];
    
    UIActivityViewController* activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:dataToShare
                                      applicationActivities:nil];
    [self presentViewController:activityViewController animated:YES completion:^{}];
}
@end
