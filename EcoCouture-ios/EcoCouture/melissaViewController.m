//
//  melissaViewController.m
//  EcoCouture
//
//  Created by Subash Luitel on 5/12/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import "melissaViewController.h"

@interface melissaViewController ()

@end

@implementation melissaViewController
@synthesize webTitle, webAddress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)officialSite:(id)sender {
    webAddress = [[NSMutableString alloc] initWithString:@"http://melissapolinar.com/"];
    [self performSegueWithIdentifier:@"melissaWeb" sender:self];
}

- (IBAction)facebook:(id)sender {
    webAddress = [[NSMutableString alloc] initWithString:@"https://www.facebook.com/melissapolinar?v=app_178091127385"];
    [self performSegueWithIdentifier:@"melissaWeb" sender:self];
}

- (IBAction)twitter:(id)sender {
    webAddress = [[NSMutableString alloc] initWithString:@"https://twitter.com/mpolinar"];
    [self performSegueWithIdentifier:@"melissaWeb" sender:self];
}

- (IBAction)youTube:(id)sender {
    webAddress = [[NSMutableString alloc] initWithString:@"http://www.youtube.com/mpolinar"];
    [self performSegueWithIdentifier:@"melissaWeb" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        WebViewController *webVC = segue.destinationViewController;
        //webVC.navigationItem.title = webTitle;
        webVC.address = webAddress;
}

@end
