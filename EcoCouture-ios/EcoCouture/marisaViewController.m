//
//  marisaViewController.m
//  EcoCouture
//
//  Created by Subash Luitel on 5/12/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import "marisaViewController.h"

@interface marisaViewController ()

@end

@implementation marisaViewController
@synthesize webAddress, webTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)officialSite:(id)sender {
    webAddress = [[NSMutableString alloc] initWithString:@"http://www.marisaq.com/"];
    [self performSegueWithIdentifier:@"marisaWeb" sender:self];
}

- (IBAction)facebook:(id)sender {
    webAddress = [[NSMutableString alloc] initWithString:@"https://www.facebook.com/Marisa.Quinn.Fanpage?fref=ts"];
    [self performSegueWithIdentifier:@"marisaWeb" sender:self];
}

- (IBAction)twitter:(id)sender {
    webAddress = [[NSMutableString alloc] initWithString:@"https://twitter.com/Marisa_Quinn"];
    [self performSegueWithIdentifier:@"marisaWeb" sender:self];
}

- (IBAction)youTube:(id)sender {
    webAddress = [[NSMutableString alloc] initWithString:@"http://www.youtube.com/user/MarisaQActress"];
    [self performSegueWithIdentifier:@"marisaWeb" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    WebViewController *webVC = segue.destinationViewController;
    //webVC.navigationItem.title = webTitle;
    webVC.address = webAddress;
}


@end
