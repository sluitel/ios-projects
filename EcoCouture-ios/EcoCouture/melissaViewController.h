//
//  melissaViewController.h
//  EcoCouture
//
//  Created by Subash Luitel on 5/12/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"

@interface melissaViewController : UIViewController
@property (strong, nonatomic) NSMutableString *webTitle;
@property (strong, nonatomic) NSMutableString *webAddress;
- (IBAction)officialSite:(id)sender;
- (IBAction)facebook:(id)sender;
- (IBAction)twitter:(id)sender;
- (IBAction)youTube:(id)sender;

@end
