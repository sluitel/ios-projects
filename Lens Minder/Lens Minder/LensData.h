//
//  LensData.h
//  Lens Minder
//
//  Created by Subash Luitel on 7/21/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LensData : NSManagedObject

@property (nonatomic, retain) NSDate * changeDate;
@property (nonatomic, retain) NSNumber * lensesLeft;
@property (nonatomic, retain) NSNumber * lensesRight;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * powerLeft;
@property (nonatomic, retain) NSNumber * powerRight;

@end
