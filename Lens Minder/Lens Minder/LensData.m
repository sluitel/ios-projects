//
//  LensData.m
//  Lens Minder
//
//  Created by Subash Luitel on 7/21/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import "LensData.h"


@implementation LensData

@dynamic changeDate;
@dynamic lensesLeft;
@dynamic lensesRight;
@dynamic id;
@dynamic powerLeft;
@dynamic powerRight;

@end
