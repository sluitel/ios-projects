//
//  ViewController.m
//  Lens Minder
//
//  Created by Subash Luitel on 7/20/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize changeContactsButton, reportLossButton, boughtNewPackButton, replacementDate, numberOfLensesLeft, numberOfLensesRight, countLeft, countRight, changeDate, changeDateString, appDelegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
	changeContactsButton.backgroundColor = [UIColor brownColor];
    reportLossButton.backgroundColor = [UIColor brownColor];
    boughtNewPackButton.backgroundColor = [UIColor brownColor];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //[self retriveData];
    //[self coreDataHasEntriesForEntityName:@"LensData"];
    [self displayContacts];
}

-(void)viewDidAppear:(BOOL)animated {
    [self retriveData];
    [self coreDataHasEntriesForEntityName:@"LensData"];
}
- (IBAction)changeContactsButtonPressed:(id)sender {
    [self retriveData];
    countRight --;
    countLeft --;
    int daysToAdd = 15;
    changeDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*daysToAdd];
    [self updateNumberOfContacts];
    [self displayContacts];
}

- (BOOL)coreDataHasEntriesForEntityName:(NSString *)entityName {
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entity];
    [request setFetchLimit:1];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count] == 0) {
        return NO;
    }
    return YES;
}

- (IBAction)reportLossButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"lossForward" sender:self];
}

- (IBAction)boughtNewPackButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"boughtNewPackSegue" sender:self];
}

- (void) displayContacts {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM dd"];
    changeDateString = [formatter stringFromDate:changeDate];
    replacementDate.text = changeDateString;
    numberOfLensesLeft.text = [NSString stringWithFormat:@"%i",countLeft];
    numberOfLensesRight.text = [NSString stringWithFormat:@"%i",countRight];
}

-(void) updateNumberOfContacts {
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    
    if (![self coreDataHasEntriesForEntityName:@"LensData"]) {
        //Insert first value into Core Data.
        LensData *firstData = [NSEntityDescription insertNewObjectForEntityForName:@"LensData"
                                                            inManagedObjectContext:context];
        firstData.id = [NSNumber numberWithInt:1];
        firstData.changeDate = [NSDate date];
        firstData.lensesLeft = [NSNumber numberWithInt:0];
        firstData.lensesRight = [NSNumber numberWithInt:0];
        firstData.powerLeft = [NSNumber numberWithFloat:4.0];
        firstData.powerRight = [NSNumber numberWithFloat:4.5];
    }
    
    // Retrieve the entity from the local store -- much like a table in a database
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LensData" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    // Set the predicate -- much like a WHERE statement in a SQL database
    NSNumber *check = [[NSNumber alloc] initWithInt:1];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", check];
    [request setPredicate:predicate];
    
    // Set the sorting -- mandatory, even if you're fetching a single record/object
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    // Request the data -- NOTE, this assumes only one match, that
    // yourIdentifyingQualifier is unique. It just grabs the first object in the array.
    
    LensData *data = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
    data.lensesRight = [NSNumber numberWithInt:countRight];
    data.lensesLeft = [NSNumber numberWithInt:countLeft];
    data.changeDate = changeDate;
    [context save:&error];
}

-(void) retriveData {
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    
    if (![self coreDataHasEntriesForEntityName:@"LensData"]) {
        //Insert first value into Core Data.
        LensData *firstData = [NSEntityDescription insertNewObjectForEntityForName:@"LensData"
                                                            inManagedObjectContext:context];
        firstData.id = [NSNumber numberWithInt:1];
        firstData.changeDate = [NSDate date];
        firstData.lensesLeft = [NSNumber numberWithInt:0];
        firstData.lensesRight = [NSNumber numberWithInt:0];
        firstData.powerLeft = [NSNumber numberWithFloat:4.0];
        firstData.powerRight = [NSNumber numberWithFloat:4.5];
        
    }
    
    // Retrieve the entity from the local store -- much like a table in a database
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LensData" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    // Set the predicate -- much like a WHERE statement in a SQL database
    NSNumber *check = [[NSNumber alloc] initWithInt:1];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", check];
    [request setPredicate:predicate];
    
    // Set the sorting -- mandatory, even if you're fetching a single record/object
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    // Request the data -- NOTE, this assumes only one match, that
    // yourIdentifyingQualifier is unique. It just grabs the first object in the array.
    
    LensData *data = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
    countLeft = [data.lensesLeft intValue];
    countRight = [data.lensesRight intValue];
    changeDate = data.changeDate;
    [context save:&error];
}

@end
