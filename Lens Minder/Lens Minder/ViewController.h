//
//  ViewController.h
//  Lens Minder
//
//  Created by Subash Luitel on 7/20/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "LensData.h"
#import <CoreData/CoreData.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *changeContactsButton;
@property (strong, nonatomic) IBOutlet UIButton *reportLossButton;
@property (strong, nonatomic) IBOutlet UIButton *boughtNewPackButton;
- (IBAction)changeContactsButtonPressed:(id)sender;
- (IBAction)reportLossButtonPressed:(id)sender;
- (IBAction)boughtNewPackButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *leftLensCount;
@property (strong, nonatomic) IBOutlet UILabel *rightLensCount;
@property (strong, nonatomic) IBOutlet UILabel *replacementDate;
@property (strong, nonatomic) IBOutlet UILabel *numberOfLensesLeft;
@property (strong, nonatomic) IBOutlet UILabel *numberOfLensesRight;
@property int countRight, countLeft;
@property (strong, nonatomic) NSDate *changeDate;
@property (strong, nonatomic) NSString *changeDateString;
@property (retain, nonatomic) AppDelegate *appDelegate;

@end
