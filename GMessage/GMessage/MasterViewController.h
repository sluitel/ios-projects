//
//  MasterViewController.h
//  GMessage
//
//  Created by Subash Luitel on 5/29/13.
//  Copyright (c) 2013 Subash Luitel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
